
# Virtuální digitální šatník - Virtuální zrcadlo
## Virtual digital wardrobe - Virtual Mirror

Virtuální zrcadlo je aplikace rozšířené reality pro zobrazování 3D rekonstruovaných modelů historického oblečení na uživateli prostřednictvím velkoplošné obrazovky. Vznikla jako dílčí výstup projektu Virtuální digitální šatník podpořeného [TA ČR](https://www.tacr.cz/), v rámci *Programu na podporu aplikovaného společenskovědního a humanitního výzkumu, experimentálního vývoje a inovací ÉTA 5*, číslo grantu [TL05000298](https://starfos.tacr.cz/projekty/TL05000298).

Interní ČVUT ID software: [CS-ASW-DCGI-2023-1](https://v3s.cvut.cz/results/detail/370149)

> Software je pro výzkumné, výukové a nekomerční projekty dostupný zdarma. Pro komerční projekty kontaktujte katedru počítačové grafiky a interakce.

| Anotace | Annotation |
| --- | --- |
| **Virtuální zrcadlo** umožňuje vizualizaci 3D modelu oblečení, získaného fotogrammetrickou rekonstrukcí nebo pomocí 3D modelování, přímo na návštěvníkovi prostřednictvím velkoplošné obrazovky. Na sekundární obrazovce (kiosek) si návštěvník může vybírat konkrétní kus oblečení. Systém magického zrcadla je navržen pomocí architektury server-klient, kdy server je aplikace zobrazující 3D model a klient je Android aplikace, která ji může ovládat. V případě nedostupnosti klientské aplikace pracuje zrcadlo v autonomním módu bez možnosti volby oblečení. Pro realizaci magického zrcadla se využivá hloubková kamera Azure Kinect, ale systém jako takový je snadno rozšiřitelný o další kamery, testováno s Kinect v2. Cílové použití aplikace je v muzejnictví pro vizualizaci historického oblečení, ale jsou zde i možnosti pro vizualizaci v návrhářství neexistujícího oblečení, nebo v obchodě jako virtuální zkušební kabinka. | **The Virtual Mirror** allows the visualization of a 3D model of clothing, obtained by photogrammetric reconstruction or 3D modeling, directly on the visitor via a large screen. On a secondary screen (kiosk), the visitor can select a specific piece of clothing. The magic mirror system is designed using a server-client architecture, where the server is the application that displays the 3D model, and the client is the Android application that can control it. In case the client application is unavailable, the mirror works in autonomous mode without the ability to select the clothes. The Azure Kinect depth camera is used to implement the magic mirror, but the system as such is easily extensible with additional cameras, tested with Kinect v2. The target use of the application is in the museum industry for visualizing historical clothing, but there are also possibilities for visualization in the fashion design of non-existent clothing or in a store as a virtual fitting room. |

### Autoři
* David Sedláček
* Ivo Malý
* Margarita Ryabova
* Jiří Jordán
* Martin Pišna
* Jiří Žára

> © 2023 Katedra počítačové grafiky a interakce, FEL, ČVUT v Praze

### Publikace
* Workflow for creating animated digital replicas of historical clothing, 2023. Submitted to Journal of Cultural Heritage.
* T. Dvořák, J. Kubišta, O. Linhart, I. Malý, D. Sedláček and S. Ubik, *Presentation of historical clothing digital replicas in motion*, in [IEEE Access](https://ieeexplore.ieee.org/document/10401907), vol. 12, pp. 13310-13326, 2024, doi: 10.1109/ACCESS.2024.3355049. **Open Access**.

## Aplikace Virtuální zrcadlo

### Vydání / Releases

* **v0_2_1** - 6.6.2023
  * [Virtuální zrcadlo](releases/virtual_mirror_v0_2_1.zip) Win10/11
  * [Virtuální zrcadlo ovládání](releases/virtual_mirror_controller_v0_2_1.apk) Android
  * src: [Virtuální zrcadlo](srccode/virtual_mirror_v0_2_1.zip) Unity project (v 2019.4.40f1)
  * src: [Virtuální zrcadlo ovládání](srccode/virtual_mirror_controller_v0_2_1.zip) Android studio project, Kotlin
* **v0_2_0** - 23.5.2023
  * [Virtuální zrcadlo](releases/virtual_mirror_v0_2_0.zip) Win10/11
* **v0_1_3** - 10.5.2023
  * [Virtuální zrcadlo](releases/virtual_mirror_v0_1_3.zip) Win10/11


### Dokumentace / Documentation
* [Programátorská dokumentace](https://tacr-satnik.pages.fel.cvut.cz/virtual-mirror/manual/README.html), [PDF](docs/VirtualMirror_project_doc.pdf)
  * [komunikační API](https://tacr-satnik.pages.fel.cvut.cz/virtual-mirror/manual/proj.remote-control.html)
  * IP adresa Virtuálního zrcadla: `http://10.39.92.156:5555/`
* Prototypy
  * [Magické zrcadlo](docs/MagicMirror_prototype.pdf) - Margaryta Ryabová
  * Klientská aplikace - Jiří Jordán

### Ukázky z použití
* Vizualizace osoby s video pozadím, 2 osoby [youtube](https://www.youtube.com/watch?v=_PV02LDsHa0)
* Vizualizace těla pomocí mračna bodů + záměna pozadí místnosti [youtube](https://www.youtube.com/watch?v=3oDSG1YsxzM)